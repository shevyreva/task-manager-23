package ru.t1.shevyreva.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.ICommandRepository;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.api.repository.IUserRepository;
import ru.t1.shevyreva.tm.api.service.*;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.command.project.*;
import ru.t1.shevyreva.tm.command.system.*;
import ru.t1.shevyreva.tm.command.task.*;
import ru.t1.shevyreva.tm.command.user.*;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.shevyreva.tm.exception.system.CommandNotSupportedException;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;
import ru.t1.shevyreva.tm.repository.CommandRepository;
import ru.t1.shevyreva.tm.repository.ProjectRepository;
import ru.t1.shevyreva.tm.repository.TaskRepository;
import ru.t1.shevyreva.tm.repository.UserRepository;
import ru.t1.shevyreva.tm.service.*;
import ru.t1.shevyreva.tm.util.TerminalUtil;

@Getter
@Setter
@NoArgsConstructor
public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationInfoCommand());
        registry(new ApplicationVersionCommand());
        registry(new ListCommand());
        registry(new ArgumentListCommand());
        registry(new ApplicationExitCommand());

        registry(new ProjectCreateCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectsShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectStartStatusByIdCommand());
        registry(new ProjectStartStatusByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectCompleteStatusByIdCommand());
        registry(new ProjectCompleteStatusByIndexCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());

        registry(new TaskCreateCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());
        registry(new TasksShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new TaskStartStatusByIdCommand());
        registry(new TaskStartStatusByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskCompleteStatusByIdCommand());
        registry(new TaskCompleteStatusByIndexCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());

        registry(new UserRegistryCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
        registry(new UserRemoveById());
        registry(new UserRemoveByEmail());
        registry(new UserRemoveByLogin());
        registry(new UserLockedCommand());
        registry(new UserUnlockedCommand());
    }

    public void registry(@Nullable final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);

        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println(e.getMessage());
                System.out.println("[FAILED]");
            }
        }
    }

    public boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

    public void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("**Welcome to Task Manager**");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("**Shutdown Task Manager**");
            }
        });
    }

    private void initDemoData() {
        userService.create("user", "user", "jack@gmail.com");
        userService.create("test", "test", "ivan@yandex.ru");
        userService.create("admin", "admin", Role.ADMIN);

        @NotNull final String userTestId = userService.findByLogin("test").getId();
        @NotNull final String userAdminId = userService.findByLogin("admin").getId();

        projectService.add(userTestId, new Project("B PROJECT", Status.COMPLETED));
        projectService.add(userTestId, new Project("A PROJECT", Status.IN_PROGRESS));
        projectService.add(userTestId, new Project("D PROJECT", Status.NON_STARTED));
        projectService.add(userAdminId, new Project("C PROJECT", Status.IN_PROGRESS));

        taskService.add(userTestId, new Task("MEGA TASK", Status.COMPLETED));
        taskService.add(userAdminId, new Task("SUPER TASK", Status.NON_STARTED));
    }

}
