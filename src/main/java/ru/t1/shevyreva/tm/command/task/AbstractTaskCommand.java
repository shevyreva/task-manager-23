package ru.t1.shevyreva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.service.IProjectTaskService;
import ru.t1.shevyreva.tm.api.service.ITaskService;
import ru.t1.shevyreva.tm.command.AbstractCommand;
import ru.t1.shevyreva.tm.enumerated.Role;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Nullable
    public ITaskService getTaskService() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getTaskService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    public IProjectTaskService getProjectTaskService() {
        if (getServiceLocator() == null) return null;
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[TASK ID]: " + task.getId());
        System.out.println("[TASK NAME]: " + task.getName());
        System.out.println("[TASK DESCRIPTION]: " + task.getDescription());
        System.out.println("[TASK STATUS]: " + Status.toName(task.getStatus()));
        System.out.println("[PROJECT ID]: " + task.getProjectId());
    }

}
