package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    void bindTaskToProject(@Nullable String userId,@Nullable String projectId,@Nullable String taskId);

    void unbindTaskToProject(@Nullable String userId,@Nullable String projectId,@Nullable String taskId);

    void removeByProjectId(@Nullable String userId,@Nullable String projectId);

    @NotNull
    List<Task> findAllTaskByProjectId(@Nullable String userId,@Nullable String projectId);

}
