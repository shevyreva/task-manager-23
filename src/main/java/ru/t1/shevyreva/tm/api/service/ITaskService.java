package ru.t1.shevyreva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.ITaskRepository;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task, ITaskRepository> {

    @NotNull
    Task create(@Nullable String userId,@Nullable String name,@Nullable String description);

    @NotNull
    Task create(@Nullable String userId,@Nullable String name);

    @NotNull
    Task updateByIndex(@Nullable String userId,@Nullable Integer index,@Nullable String name,@Nullable String description);

    @NotNull
    Task updateById(@Nullable String userId,@Nullable String id,@Nullable String name,@Nullable String description);

    @NotNull
    Task changeTaskStatusByIndex(@Nullable String userId,@Nullable Integer index,@Nullable Status status);

    @NotNull
    Task changeTaskStatusById(@Nullable String userId,@Nullable String id,@Nullable Status status);

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId,@Nullable String projectId);

    List<Task> findAll(@Nullable String userId,@Nullable TaskSort sort);

}
