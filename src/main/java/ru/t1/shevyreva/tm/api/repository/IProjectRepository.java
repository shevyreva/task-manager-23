package ru.t1.shevyreva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.TaskSort;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.model.Task;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(@NotNull String userId,@NotNull String name,@NotNull String description);

    @NotNull
    Project create(@NotNull String userId,@NotNull String name);

    @NotNull
    List<Project> findAll(@NotNull String userId,@NotNull ProjectSort sort);

}
