package ru.t1.shevyreva.tm.api.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.enumerated.Status;

public interface IHaveStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
