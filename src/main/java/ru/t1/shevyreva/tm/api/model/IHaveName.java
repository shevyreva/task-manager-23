package ru.t1.shevyreva.tm.api.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IHaveName {

    @NotNull
    String getName();

    void setName(@NotNull String name);

}
