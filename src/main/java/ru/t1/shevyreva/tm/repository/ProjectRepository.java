package ru.t1.shevyreva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.IProjectRepository;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.exception.field.UserIdEmptyException;
import ru.t1.shevyreva.tm.model.Project;

import java.util.List;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(userId, project);
    }

    @NotNull
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Project project = new Project();
        project.setName(name);
        return add(userId, project);
    }

    @NotNull
    public List<Project> findAll(@NotNull final String userId,@NotNull final ProjectSort sort) {
        return findAll(userId, sort.getComparator());
    }

}
